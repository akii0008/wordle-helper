import commandLineArgs, { OptionDefinition } from 'command-line-args';
import pkg from '../package.json';
import { readFileSync } from 'fs';
import path from 'path';

// Green-colored letters
function placement(input: string) {
  const regex = new RegExp('\[[a-z],[0-9]\]', 'gi'); // eslint-disable-line no-useless-escape
  const matches = input.match(regex);

  if (matches === null) throw 'Invalid or missing placement';
  if (matches.length !== 1) throw new Error(`Too many placements in one flag. Split them across multiple \`-p\`'s. Input: ${input}`);

  const place = matches.map(match => [match.substring(0, 1), +match.substring(2, 3)])[0];

  return place;
}

// Yellow-colored letters
function include(input: string) {
  const regex = new RegExp('[a-z]', 'gi');
  const matches = [...input.matchAll(regex)].map(g => g[0]);

  if (matches === null) throw new Error('Missing letters that the wordle has');

  return matches.join('');
}

// Black-colored letters
function exclude(input: string) {
  const regex = new RegExp('[a-z]', 'gi');
  const matches = [...input.matchAll(regex)].map(g => g[0]);

  if (matches === null) throw new Error('Missing exclusions');

  return matches.join('');
}

const optionDefinitions: OptionDefinition[] = [
  { name: 'help', type: Boolean },
  { name: 'version', alias: 'v', type: Boolean },

  { name: 'place', alias: 'p', type: placement, multiple: true },
  { name: 'include', alias: 'i', type: include },
  { name: 'exclude', alias: 'e', type: exclude }
];
const options = commandLineArgs(optionDefinitions);

console.log('options', options);

if (options.version) {
  console.log(`wordle-helper v${pkg.version}`);
  process.exit();
}

if (options.help) {
  console.log(`Help: wordle-helper - v${pkg.version}`);
  console.log('Usage: wordle-helper <options>');
  console.log(' ');

  console.log('   --help : Displays this message and exits');
  console.log(' ');

  console.log('   -e | --exclude : Letters to exclude from the search.');
  console.log('                    Example: -e abc');
  console.log('   -h | --has     : Letters that the wordle has, but placements are unknown.');
  console.log('                    Example: -h abc');
  console.log('   -p | --place   : A letter and where it is in the word.');
  console.log('                    Example: -p [a,1] or, if there are many, -p [a,1] -p [b,2]');
  console.log(' ');

  console.log('(This tool does NOT have Super Cow Powers.)');

  process.exit();
}

const dictionary = readFileSync(path.join(__dirname, './dictionary.txt')).toString().split('\n').map(g => g.toLowerCase());

let filtered = dictionary;

if (!options.place && !options.include && !options.exclude) {
  console.error('No options. Exiting...');
  process.exit();
}

if (options.exclude) {
  const characters: string[] = options.exclude.split('');
  filtered = filtered.filter(word => {
    return characters.every(character => !word.includes(character));
  });
}

if (options.include) {
  const characters: string[] = options.include.split('');
  filtered = filtered.filter(word => {
    return characters.every(character => word.includes(character));
  });
}

if (options.place) {
  for (const [character, index] of options.place) {
    filtered = filtered.filter(word => {
      return word.slice(index - 1, index) === character;
    });
  }
}

console.log(filtered.join(', '));
