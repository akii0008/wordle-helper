# Wordle Helper

A little tool to help those of us who find it difficult to remember the english language. Will output possible words given constraints of what the letters are, aren't, and where they are in the word.

## Download
* [Windows](https://gitlab.com/akii0008/wordle-helper/-/jobs/artifacts/main/raw/dist/wordle-helper-win.exe?job=build-and-dist)
* [macOS](https://gitlab.com/akii0008/wordle-helper/-/jobs/artifacts/main/raw/dist/wordle-helper-macos?job=build-and-dist)
* [Linux (any x64)](https://gitlab.com/akii0008/wordle-helper/-/jobs/artifacts/main/raw/dist/wordle-helper-linux?job=build-and-dist)

## Usage:
Run the file you downloaded via your terminal.<br>
For help, check `/path/to/file --help`

* `--include` (`-i`): Yellow letters. `--include abcde`
* `--exclude` (`-e`): Black letters. `--exclude abcde`
* `--place` (`-p`): Green letters. first letter starts at 1, not 0. `--place [a,1] --place [b,2]`

Example:
* `/path/to/file --exclude abcde --include defgh --place [i,1] --place [z,5]`